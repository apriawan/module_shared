import 'package:module_shared/src/core/enum/enum.dart';

import '../../model/response.model.dart';

abstract class IConnect {
  Future<ApiResponse> get({required ModuleType modul, required String path, Map<String, dynamic>? query});
  Future<ApiResponse> post({required ModuleType modul, required String path, required dynamic body});
  Future<ApiResponse> put({required ModuleType modul, required String path, required dynamic body});
  Future<ApiResponse> patch({required ModuleType modul, required String path, required dynamic body});
  Future<ApiResponse> delete({required ModuleType modul, required String path, required dynamic id});
}
