class FileConstant {
  static const String faqListEn = "assets/json/faq_en.json";
  static const String faqListId = "assets/json/faq_id.json";
  static const String countryList = "assets/json/countryList.json";
  static const String provinceList = "assets/json/provinceList.json";
}
