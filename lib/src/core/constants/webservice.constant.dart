import 'package:flutter_dotenv/flutter_dotenv.dart';

class WebServiceConstant {
  static final String rootUrl = dotenv.env['rootUrl']!;
  static final String rootUrlGeneral = dotenv.env['rootUrl.general']!;
  static final String rootUrlNotification = dotenv.env['rootUrl.notification']!;
  static final String rootUrlLeaveHO = dotenv.env['rootUrl.leaveHO']!;
  static final String rootUrlTravel = dotenv.env['rootUrl.travel']!;
  static final String rootUrlWorkflow = dotenv.env['rootUrl.workflow']!;
  static final String rootUrlIzin = dotenv.env['rootUrl.izin']!;
  static final String rootUrlHeregistrasi = dotenv.env['rootUrl.heregistrasi']!;
  static final String rootUrlBenefit = dotenv.env['rootUrl.benefit']!;
  static final String rootUrlDeclaration = dotenv.env['rootUrl.declaration']!;
  static final String rootUrlSpl = dotenv.env['rootUrl.workhour']!;
  static final String rootUrlPerformance = dotenv.env['rootUrl.performance']!;

  // General
  static const String getEmployeeInformation = 'api/GeneralMasterEmployee/GetEmployeeInformationByParam';
  static const getFamilyList = 'api/GeneralMasterEmployeeDependent/getFamilyList';
  static const getModuleByLogin = 'api/GeneralMasterLoginMobile/GetAllByParam';

  // Workflow
  static const String getTaskHistory = 'api/TaskHistory/GetHistory';
  static const String getTaskArchive = 'api/TaskHistory/GetArchive';
  static const String getAllTask = 'api/RunTask/MyRequest';
  static const String getApprovalCount = 'api/RunTask/TaskApproval';
  static const String getApprovalList = 'api/RunTask/MyApproval';
  static const String getAllWorkFlowTask = 'api/RunTask/LogActivity';
  static const String getTaskOnProgress = '/api/RunTask/GetTaskOnProgress';
  static const String getTaskDraft = '/api/RunTask/GetDraft';

  // Notification
  static const String getNotification = '/PushNotification/GetNotification';
  static const String subscribeNotification = '/PushNotification/Subscribe';
  static const String unsubscribeNotification = '/PushNotification/Unsubscribe';
  static const String markAsReadNotification = '/PushNotification/MarkAsRead';

  // Leave HO
  // static const getAllLeaveRequest = 'api/leaverequest/getAllLeaveRequestData';
  static const getAllLeaveRequest = 'api/leaverequest/getAllLeaveRequestData';
  static const getLeaveRequestDetail = 'api/LeaveHeader/GetLeaveInformationByParam';
  static const String getJumlahHariFree = 'api/JumlahHariFree/GetJumlahHariFree';
  static const String getLeaveHeaderbyDate = 'api/LeaveHeader/GetLeaveHeaderByDate';
  static const String createLeaveRequest = 'api/LeaveHeader/CreateLeaveRequest';
  static const String resubmitLeaveRequest = 'api/LeaveHeader/ResubmitLeaveRequest';
  static const String submitDraftLeaveRequest = 'api/LeaveHeader/DraftSubmitLeaveRequest';
  static const String createDraftLeaveRequest = 'api/LeaveHeader/DraftLeaveRequest';
  static const String updateDraftLeaveRequest = 'api/LeaveHeader/SaveDraftLeaveRequest';
  static const String deleteDraft = 'api/LeaveHeader/deleteDraft';
  static const String cancelLeaveRequest = 'api/LeaveHeader/CancelLeaveRequest';
  static const String updateLRStatus = 'api/LeaveRequest/UpdateLRStatus';

  //Leave Site
  static const getLeaveSiteRequestDetail = 'api/LeaveHeader/GetLeaveSiteInformationByParam';
  static const getAktivitasList = 'api/LeaveSiteMaster/getAktivitasList';

  static const getMasterFlightData = 'api/MasterDataFlight/GetAllMasterDataFlight';
  static const getMasterFlightDataLoc = 'api/MasterDataFlight/GetAllFlightLocation';
  static const getMasterFlightDataByLoc = 'api/MasterDataFlight/GetAllMasterDataFlightByLoc';

  static const getMasterHotelData = 'api/MasterDataHotel/GetAllMasterDataHotel';
  static const getMasterHotelDataLoc = 'api/MasterDataHotel/GetAllMasterDataHotelLoc';
  static const getMasterHotelDataByLoc = 'api/MasterDataHotel/GetAllMasterDataHotelByLoc';

  static const getMasterCarData = 'api/MasterDataCar/GetAllMasterDataCar';
  static const getMasterCarDataLoc = 'api/MasterDataCar/GetAllCarLocation';
  static const getMasterCarDataByLoc = 'api/MasterDataCar/GetAllMasterDataCarByLoc';

  static const isTripOnProgress = 'api/LeaveSiteMaster/isTripOnProgress';
  static const isTripExist = 'api/LeaveSiteMaster/isTripExsist';

  static const createDraftLeaveRequestSite = 'api/LeaveHeader/DraftLeaveRequestSite';
  static const createLeaveRequestSite = 'api/LeaveHeader/CreateLeaveRequestSite';
  // static const createLeaveSiteReservation = 'api/LeaveHeader/CreateLeaveSiteReservation';
  static const createLeaveSiteReservation = 'api/LeaveHeader/CreateLeaveSiteReservationTvlk';

  static const getLeaveTravelGSTransport = 'api/LeaveTravelGSTransportasi/GetLeaveTravelInformationByParam'; //Lampiran JPG tiket
  static const getTravelGSTransport = 'api/TravelGSTransportation/GetTravelInformationByParam';
  static const fetchLampiran = 'https://bspace-uat.bukitmakmur.com/CallGSUpload/Download?imagename=';

  //Leave Site - Additional Ticket
  static const getLeaveTravelHeader = 'api/LeaveHeader/GetLeaveTraverHeaderByEmployeeID';
  static const getTransportDataTicket = 'api/LeaveHeader/GetLeaveDetailTransportHistoryByHeaderID';

  static const getTransportAdditionalTicket = 'api/LeaveHeader/GetDataTransportAddTicket';
  static const getLeaveHeaderAddTicket = 'api/LeaveHeader/GetLeaveHeaderAddTicketByID';
  static const getDataDws = 'api/LeaveHeader/GetDataDWS';
  static const submitAdditionalTicket = 'api/LeaveHeader/CreateLeaveHeaderAddTicketSubmit';

  // Izin
  static const String getCountFreeDay = 'api/LeavePermissionRequest/GetCountFreeDay';
  static const String getAllPermissionCategory = 'api/GeneralMasterPermissionCategory/GetAllByParam';
  static const String getAllPermissionType = 'api/GeneralMasterPermissionType/GetAllByParam';
  static const String selectByCategoryEmployeeID = 'api/GeneralMasterPermissionType/GetByCategoryEmployeeID';
  static const String searchPermissionTypeByName = 'api/GeneralMasterPermissionType/SearchPermissionTypeByName';
  static const String getPermissionTypeByParam = 'api/GeneralMasterPermissionType/GetPermissionTypeByParam'; //baru
  static const String draftPermissionRequest = 'api/LeavePermissionRequest/Draft';
  static const String submitPermissionRequest = 'api/LeavePermissionRequest/Submit';
  static const String resubmitPermissionRequest = 'api/LeavePermissionRequest/Revise';
  static const String deleteDraftPermissionRequest = 'api/LeavePermissionRequest/DeleteDraft';
  static const String submitDraftPermissionRequest = 'api/LeavePermissionRequest/SubmitDraft';
  static const String rejectPermissionRequest = 'api/LeavePermissionRequest/Reject';
  static const String approvePermissionRequest = 'api/LeavePermissionRequest/Approve';
  static const String revisePermissionRequest = 'api/LeavePermissionRequest/AskForRevision';
  static const String getDetailDraftSave = 'api/LeavePermissionRequest/GetDetailByID';
  static const String getInformationCuti = 'api/LeaveHeader/GetLeaveInformationByParam';
  static const String getcategoryByEmployeeID = 'api/GeneralMasterPermissionType/GetByCategoryEmployeeID';
  static const String getDetailLeavePermissionRequest = 'api/LeavePermissionRequest/GetDetailByRunWorkflowID';
  static const String uploadPermissionAttachment = 'api/LeavePermissionAttachment/UploadFileAttachment';
  static const String getInfoFilePermissionAttachment = 'api/LeavePermissionAttachment/GetAllByParam';
  static const String getFilePermissionAttachment = 'api/LeavePermissionAttachment/GetFileAttachment';
  static const String getCountFilePermissionAttachment = 'api/api/LeavePermissionAttachment/GetCountFileAttachment';
  static const String updateIzinStatus = 'api/LeavePermissionRequest/';

  //Heregistrasi
  static const String getHeregistrasiAll = 'api/Heregistration/GetDataEmployee';
  static const String getDraftAll = 'api/Heregistration/GetDataDraft';
  static const String draftHeregistrasiRequest = 'api/Heregistration/Draft';
  static const String submitHeregistrasiRequest = 'api/Heregistration/SubmitDraft';
  static const String getFileAttachmentHeregistrasi = 'api/HregRequestAttachment/GetDataAttachmentByHeaderID';
  static const String getMasterIndustry = 'api/Heregistration/GetMasterIndustry';
  static const String getMasterPosition = 'api/Heregistration/GetMasterPosition';
  static const String getMasterMajor = 'api/GeneralMasterMajor';
  static const String getMasterMajorMapping = 'api/GeneralMasterMajor/GetMajorMapping';
  static const String getMasterBank = 'api/GeneralMasterBank';
  static const String previewHeregistrasi = 'api/Heregistration/GetDataPreview';
  static const String previewReviseHeregistrasi = 'api/Heregistration/GetDataRevisePreview';
  static const String uploadFileHeregistrasi = 'api/HregRequestAttachment/UploadFileAttachment';
  static const String getDetailTransactionHeregistrasi = 'api/Heregistration/GetDataDetailTransaction';
  static const String getDataTransactionHeregistrasi = 'api/Heregistration/GetDataTransaction';
  static const String resubmitHeregistrasiRequest = 'api/Heregistration/Resubmit';
  static const String reviseUpdateHeregistrasiRequest = 'api/Heregistration/ReviseUpdate';
  static const String downloadFileHeregistrasiRequest = 'api/HregRequestAttachment/Download';

  // Travel
  static const String masterTravelActivityType = 'api/TravelMasterActivity/GetActivities';
  static const String masterTravelAdvanceType = 'api/TravelMasterAdvanceType';
  static const String getTravelRequest = 'api/travelrequest/getAllTravelRequestData';
  static const String getAllTravelRequestTvlk = 'api/travelrequest/getAllTravelRequestDataTvlk';
  static const String saveDraftTravelRequest = 'api/TravelHeader/DraftTravelRequest';
  static const String resaveDraftTravelRequest = 'api/TravelHeader/SaveDraftTravelRequest';
  static const String deleteDraftTravelRequest = 'api/TravelHeader/DeleteDraft';
  static const String submitTravelRequest = 'api/TravelHeader/CreateTravelRequest';
  static const String resubmitTravelRequest = 'api/TravelHeader/ResubmitTravelRequest';
  static const String draftSubmitTravelRequest = 'api/TravelHeader/DraftSubmitTravelRequest';
  static const String updateTravelApprovalStatus = 'api/TravelRequest/UpdateTRStatus';
  static const String submitBookingTravelRequest = 'api/TravelHeader/CreateTravelReservation';
  static const String submitBookingTravelTravelokaRequest = 'api/TravelHeader/CreateTravelReservationTvlk';

  //Traveloka
  // static const String flightOneWayByParam = 'api/TravelokaMaster/Traveloka/FlightOneWayByParam';
  static const String flightOneWayByParam = 'api/TravelokaMaster/Traveloka/FlightOneWayByParamv2';
  static const String getBaggageInfo = 'api/TravelokaMaster/Traveloka/getBaggageInfoByParam';
  static const String getHotelListByParam = 'api/TravelokaMaster/Traveloka/getHotelListByParam';
  static const String getRoomListByParam = 'api/TravelokaMaster/Traveloka/getRoomListByParam';
  static const String getAreaByParam = 'api/TravelokaMaster/Traveloka/getAreaByParam';
  //Travel - Additional Ticket
  static const getTravelHeader = 'api/TravelHeader/GetTravelHeaderByEmployeeID';
  static const getTravelTransportDataTicket = 'api/TravelHeader/GetTravelDetailTransportHistoryByHeaderID';

  static const getTravelTransportAdditionalTicket = 'api/TravelHeader/GetDataTransportAddTicket';
  static const getTravelHeaderAddTicket = 'api/TravelHeader/GetTravelHeaderAddTicketByID';
  static const getTravelDataDws = 'api/TravelHeader/GetDataDWS';
  static const submitTravelAdditionalTicket = 'api/TravelHeader/CreateTravelHeaderAddTicketSubmit';
  static const cancelRequestTravel = 'api/TravelHeader/CancelTravelRequest';

  // Benefit
  static const String updateBRStatus = 'api/BenefitRequest/UpdateBRStatus';
  static const String benefitAttachment = 'api/BenefitAttachment/Download/';
  // Benefit Grief
  static const String familyMembersForBenefitGrief = 'api/GeneralMasterEmployeeDependent/FamilyMembersForBenefitGrief';
  static const String benefitGriefByEmployeeID = 'api/BenefitGrief/BenefitGriefByEmployeeID';
  static const String submitBenefitGrief = 'api/BenefitGrief/CreateBenefitGrief';
  static const String submitBenefitGriefWithAttachments = 'api/BenefitGrief/CreateBenefitGriefWithAttachments';
  static const String getBenefitGriefByID = 'api/BenefitGrief/getBenefitGriefByID';
  static const String resubmitBenefitGrief = 'api/BenefitGrief/ResubmitBenefitGrief';
  static const String resubmitBenefitGriefWithAttachments = 'api/BenefitGrief/ResubmitBenefitGriefWithAttachments';
  static const String draftBenefitGrief = 'api/BenefitGrief/DraftBenefitGrief';
  static const String draftBenefitGriefWithAttachments = 'api/BenefitGrief/DraftBenefitGriefWithAttachments';
  static const String saveDraftBenefitGrief = 'api/BenefitGrief/SaveDraftBenefitGrief';
  static const String saveDraftBenefitGriefWithAttachments = 'api/BenefitGrief/SaveDraftBenefitGriefWithAttachments';
  static const String draftSubmitBenefitGrief = 'api/BenefitGrief/DraftSubmitBenefitGrief';
  static const String draftSubmitBenefitGriefWithAttachments = 'api/BenefitGrief/DraftSubmitBenefitGriefWithAttachments';
  static const String deleteDraftBenefitGrief = 'api/BenefitGrief/DeleteDraftBenefitGrief';
  // Benefit Marriage
  static const String eligibilityBenefitMarriage = 'api/BenefitMarriage/EligibilityBenefitMarriage';
  static const String submitBenefitMarriage = 'api/BenefitMarriage/CreateBenefitMarriage';
  static const String submitBenefitMarriageWithAttachments = 'api/BenefitMarriage/CreateBenefitMarriageWithAttachments';
  static const String reSubmitBenefitMarriageWithAttachments = 'api/BenefitMarriage/ResubmitBenefitMarriageWithAttachments';
  static const String getBenefitMarriageByID = 'api/BenefitMarriage/GetBenefitMarriageByID';
  static const String draftBenefitMarriageWithAttachments = 'api/BenefitMarriage/DraftBenefitMarriageWithAttachments';
  static const String draftSubmitBenefitMarriageWithAttachments = 'api/BenefitMarriage/DraftSubmitBenefitMarriageWithAttachments';
  static const String saveDraftBenefitMarriageWithAttachments = 'api/BenefitMarriage/SaveDraftBenefitMarriageWithAttachments';
  static const String deleteDraftBenefitMarriage = 'api/BenefitMarriage/DeleteDraftBenefitMarriage';
  // Benefit Disaster
  static const String benefitDisasterCreateInfo = 'api/BenefitDisaster/GetInfoBenefitDisaster';
  static const String submitBenefitDisasterWithAttachments = 'api/BenefitDisaster/CreateBenefitDisasterWithAttachments';
  static const String reSubmitBenefitDisasterWithAttachments = 'api/BenefitDisaster/ResubmitBenefitDisasterWithAttachments';
  static const String getBenefitDisasterByID = 'api/BenefitDisaster/GetBenefitDisasterByID';
  static const String draftBenefitDisasterWithAttachments = 'api/BenefitDisaster/DraftBenefitDisasterWithAttachments';
  static const String draftSubmitBenefitDisasterWithAttachments = 'api/BenefitDisaster/DraftSubmitBenefitDisasterWithAttachments';
  static const String saveDraftBenefitDisasterWithAttachments = 'api/BenefitDisaster/SaveDraftBenefitDisasterWithAttachments';
  static const String deleteDraftBenefitDisaster = 'api/BenefitDisaster/DeleteDraftBenefitDisaster';
  // Benefit Great Leave
  static const String eligibilityBenefitGreatLeave = 'api/BenefitGreatLeave/GetInfoBenefitGreatLeave';
  static const String submitBenefitGreatLeave = 'api/BenefitGreatLeave/CreateBenefitGreatLeave';
  static const String reSubmitBenefitGreatLeave = 'api/BenefitGreatLeave/ResubmitBenefitGreatLeave';
  static const String getBenefitGreatLeaveByID = 'api/BenefitGreatLeave/GetBenefitGreatLeaveByID';
  static const String draftBenefitGreatLeave = 'api/BenefitGreatLeave/DraftBenefitGreatLeave';
  static const String draftSubmitBenefitGreatLeave = 'api/BenefitGreatLeave/DraftSubmitBenefitGreatLeave';
  static const String saveDraftBenefitGreatLeave = 'api/BenefitGreatLeave/SaveDraftBenefitGreatLeave';
  static const String deleteDraftBenefitGreatLeave = 'api/BenefitGreatLeave/DeleteDraftBenefitGreatLeave';

  //Declaration
  static const String getDeclarationMasterItem = 'api/DeclarationMasterItem';
  static const String getDeclarationRequestList = 'api/DeclarationRequest/GetRequestList';
  static const String getDeclarationActivityByTravelId = 'api/DeclarationRequest/GetActivityDetailByTravelID';
  static const String getDeclarationActivityByWorkflowId = 'api/DeclarationRequest/GetActivityDetailByRunWorkflowID';
  static const String getDeclarationAdvance = 'api/DeclarationRequest/GetAdvanceDetailByTravelID';
  static const String getDeclarationCalculation = 'api/DeclarationRequest/GetCalculationByTravelID';
  static const String getDeclarationDetail = 'api/DeclarationRequest/GetDeclarationDetailByHeaderID';

  static const String getActivityByTravelID = 'api/DeclarationRequest/GetActivityDetailByTravelID';
  static const String getActivityByRunWorkflowID = 'api/DeclarationRequest/GetActivityDetailByRunWorkflowID';
  static const String getAdvanceByTravelID = 'api/DeclarationRequest/GetAdvanceDetailByTravelID';

  static const String getMinusAdvanceList = 'api/DeclarationRequest/GetMinusAdvanceList';
  static const String getFileAttachment = 'api/DeclarationRequest/GetFileAttachment';
  static const String uploadDeclarationAttachment = 'api/DeclarationRequest/UploadDeclarationDetailAttachment';
  static const downloadDeclarationAttachment = 'api/DeclarationRequest/GetFileAttachment';

  static const String deleteDeclarationAttachment = 'api/DeclarationRequest/DeleteDeclarationDetailAttachment';
  static const String submitRequestDeclaration = 'api/DeclarationRequest/Submit';
  static const String createDraftRequestDeclaration = 'api/DeclarationRequest/Draft';
  static const String submitMinusAdvance = 'api/DeclarationRequest/MinusAdvanceProcess';
  static const String submitDraftRequestDeclaration = 'api/DeclarationRequest/SubmitDraft';
  static const String deleteDraftRequestDeclaration = 'api/DeclarationRequest/DeleteDraft';
  static const String resubmitRequestDeclaration = 'api/DeclarationRequest/Revise';
  static const String reviseRequestDeclaration = 'api/DeclarationRequest/AskForRevision';
  static const String approveRequestDeclaration = 'api/DeclarationRequest/Approve';
  static const String rejectRequestDeclaration = 'api/DeclarationRequest/Reject';
  static const String getDeclarationPlafonLimit = 'api/DeclarationMasterItemPlafon/GetItemPlafonListByTravelID';

  //Leave Site - Refund Request
  static const String getRefundRequestByEmployeeID = 'api/RefundRequest/GetRefundRequestByEmployeeID';
  static const String getCompletedLeaveHeaderByEmployeeId = 'api/LeaveHeader/GetCompletedLeaveHeaderByEmployeeID';
  static const String getLeaveHeaderById = 'api/LeaveHeader/GetLeaveHeaderByID';
  static const String getLeaveGSFlightByTransactionID = 'api/LeaveTravelGSTransportasi/GetLeaveGSFlightByTransactionID';
  static const String getLeaveDetailTransportationByHeaderID = 'api/LeaveDetailTransportation/GetLeaveDetailTransportationByHeaderID';
  static const String createLeaveHeaderRefundSubmit = 'api/LeaveHeader/CreateLeaveHeaderRefundSubmit';
  static const String resubmitLeaveHeaderRefund = 'api/LeaveHeader/ResubmitLeaveHeaderRefund';
  static const String addRefundRequest = 'api/RefundRequest/AddRefundRequest';
  static const String updateRefundRequest = 'api/RefundRequest/UpdateRefundRequest';
  static const String updateLeaveGsList = 'api/LeaveTravelGSTransportasi/UpdateLeaveGSList';
  static const String getLeaveRefundRequestByTransactionId = 'api/RefundRequest/GetRefundRequestByTransactionID';

  //SPL
  static const String getOverTimeType = 'api/WorkHourOverTimeType';
  static const String getReceiverList = 'api/WorkHourOverTime/GetReceiverDwsInfoList';
  static const String getDwsInfo = 'api/WorkHourOverTime/GetDWSInfo';
  static const String getDwsTime = 'api/WorkHourOverTime/GetDWSTime';
  static const String getOverTimeList = 'api/WorkHourOverTime/GetOverTimeList';
  static const String getOverTimeById = 'api/WorkHourOverTime/GetOverTimeByID';
  static const String getOverTimeByRunWorkflowId = 'api/WorkHourOverTime/GetOverTimeByRunWorkflowID';
  static const String submitSplRequest = 'api/WorkHourOverTime/Submit';
  static const String draftSplRequest = 'api/WorkHourOverTime/Draft';
  static const String submitDraftSplRequest = 'api/WorkHourOverTime/SubmitDraft';
  static const String deleteDraftSpl = 'api/WorkHourOverTime/DeleteDraft';

  // Travel - Refund Request
  static const String getCompletedTravelHeaderByEmployeeId = 'api/TravelHeader/GetCompletedTravelHeaderByEmployeeID';
  static const String getTravelHeaderById = 'api/TravelHeader/GetTravelHeaderByID';
  static const String getTravelGSFlightByTransactionID = 'api/TravelGSTransportation/GetTravelGSFlightByTransactionID';
  static const String getTravelDetailTransportationByHeaderID = 'api/TravelDetailTransportation/GetTravelDetailTransportationByHeaderID';
  static const String createTravelHeaderRefundSubmit = 'api/TravelHeader/CreateTravelHeaderRefundSubmit';
  static const String resubmitTravelHeaderRefund = 'api/TravelHeader/ResubmitTravelHeaderRefund';
  static const String addTravelRefundRequest = 'api/RefundRequest/AddRefundRequest';
  static const String updateTravelRefundRequest = 'api/RefundRequest/UpdateRefundRequest';
  static const String updateTravelGsList = 'api/TravelGSTransportation/UpdateTravelGSList';
  static const String getTravelRefundRequestByTransactionId = 'api/RefundRequest/GetRefundRequestByTransactionID';

  // Performance - Feedback
  static const String getListEmployeeFeedback = "api/Feedback/GetListEmployee";
  static const String askFeedback = "api/Feedback/AskFeedback";
  static const String giveFeedback = "api/Feedback/GiveFeedback";
  static const String giveFeedbackFromRequest = "api/Feedback/GiveFeedbackFromRequest";
  static const String draftFeedback = "api/Feedback/Draft";
  static const String saveFeedback = "api/Feedback/SaveDraft";
  static const String deleteFeedback = "api/Feedback/DeleteDraft";
  static const String submitFeedback = "api/Feedback/SubmitDraft";
  static const String feedbackToTou = "api/Feedback/FeedbackToYou";
  static const String feedbackToOthers = "api/Feedback/FeedbackToOthers";
  static const String feedbackRequestFromOthers = "api/Feedback/FeedbackRequestFromOthers";
  static const String myDraftFeedback = "api/Feedback/MyDraftFeedback";
  static const String getFeedbackByID = "api/Feedback/GetByID";
  static const String getGuideFeedback = "api/FeedbackGuide";

  static const String addReply = "api/Feedback/AddReply";
  static const String updateRead = "api/Feedback/UpdateRead";

  //Performance - IPD
  static const String getIPDHeader = "api/IPDRequest/IPD/Inquiry";

  static const String ipdHeader = "api/IPDRequest/IPD";
  static const String ipdKpiActivity = "api/IPDRequest/KPI-Activity/Inquiry";
  static const String leadToDirectKPI = "api/IPDRequest/LeadToDirectKPI";
  static const String kpiItemCatalog = "api/GeneralMasterKPICatalog/Inquiry";
  static const String competencyCatalog = "api/GeneralMasterCompetencyCatalog/Inquiry";
  static const String ipdKpiBatchUpsert = "api/IPDRequest/KPI-Activity/BatchUpsert";
  static const String getDevelopment = "api/Development/Inquiry";
  static const String ipdSubmitToWorkflow = "api/IPDRequest/Submit";
  static const String batchUpsertDevelopment = "api/Development/BatchUpsert";
  static const String eligibilityIPD = "api/IPDRequest/IPD/Eligible";
  static const String approveIPD = "api/IPDRequest/Approve";
  static const String rejectIPD = "api/IPDRequest/Reject";
  static const String reviseIPD = "api/IPDRequest/AskForRevision";
  static const String kpiProject = "api/GeneralMasterKPIProject/Inquiry";
  static const String monitoringIPD = "api/IPDActivity/BatchUpdate";
  static const String closeMonitoringIPD = "api/IPDRequest/Monitoring/Closing";
  static const String submitFinalIPD = "api/IPDRequest/Final/Submit";
  static const String approveFinalIPD = "api/IPDRequest/Final/Approve";
  static const String kpiCompanySite = "api/GeneralMasterKPITargetPlan/Inquiry";
  static const String getProjectMemberCR = "api/GeneralMasterKPIProject/ProjectMemberCR";

  //ipd tambahan eki
  static const String autoAddKpiIpd = "api/IPDRequest/KPI-Activity/BatchUpsert";
  static const String getIpdKpiActivity = "api/IPDRequest/KPI-Activity/Inquiry";
  static const String deleteKPIItemIpd = "api/IPDRequest/DeleteKPIItem";
  static const String deleteKeyActivityIpd = "api/IPDRequest/DeleteKeyActivity";
  static const String getIpdActivityByKPIPlanID = "api/IPDRequest/IPDActivityByKPIPlanID";
  static const String submitDataIPD = "api/IPDRequest/submit";
  static const String ipdAskForRevision = "api/IPDRequest/AskForRevision";
  static const String ipdApprove = "api/IPDRequest/Approve";
  static const String getIPDHeaderByIPDNo = "api/IPDRequest/IPDRequestByIPDNo";
  static const String getIPDHeaderByIPDCode = "api/IPDRequest/IPDRequestByIPDCode";
  static const String ipdCloseMonitoring = "api/IPDRequest/Monitoring/Closing";
  static const String submitAchFinalIpd = "api/IPDRequest/Final/Submit";
  static const String getIpdActivityDb = "api/IPDRequest/IPDActivityByActivityID";
  static const String batchSubmitAchFinalIpd = "api/IPDActivity/BatchUpdate";
  static const String batchapproveAchFinalIpd = "api/IPDActivity/BatchUpdate";
  static const String ipdBatchUpdate = "api/IPDActivity/BatchUpdate";
  static const String ipdGetDirect = "api/IPDRequest/IPDGetDirectHomebaseDirectProject";
  static const String ipdKpiUpdate = "api/IPDKPI/BatchUpdate";

  // Performance - Coaching
  static const String getTopicCoaching = "api/CoachingActivityDetail/GetTopic";
  static const String getEmployeeCoaching = "api/CoachingActivityDetail/GetEmployee";
  static const String getEmployeeCoachingBySearchEmployeeIDName = "api/CoachingActivityDetail/GetEmployeeBySearchEmployeeIDName";
  static const String getCoachingActivityDetailByCoachingID = "api/CoachingActivityDetail/GetCoachingActivityDetailByCoachingID";
  static const String getCoachingActivityHeaderByCoachingID = "api/CoachingActivityHeader/GetCoachingActivityHeaderByCoachingID";
  static const String createDetailAskCoaching = "api/CoachingActivityDetail/CreateDetailAskCoaching";
  static const String getCoachingActivityHeaderByEmployeeIDCoachee = "api/CoachingActivityHeader/GetCoachingActivityHeaderByEmployeeIDCoachee";
  static const String getCoachingActivityHeaderByEmployeeIDCoachStatusWaiting =
      "api/CoachingActivityHeader/GetCoachingActivityHeaderByEmployeeIDCoachStatusWaiting";
  static const String getCoachingActivityHeaderByCreatedBy = "api/CoachingActivityHeader/GetCoachingActivityHeaderByCreatedBy";

  static const String getCoachingDirectHomebase = 'api/CoachingActivityHeader/GetDirectHomebaseByEmployeeIDCoachee';

  static const String createGiveCoaching = "api/CoachingActivityHeader/CreateGiveCoaching";
  static const String createAskCoaching = "api/CoachingActivityHeader/CreateAskCoaching";
  static const String cekDataAskCoaching = "api/CoachingActivityHeader/cekDataAskCoaching";
  static const String cekSubordinateDirectHomebaseByCoachCoachee = "api/CoachingActivityHeader/CekSubordinateDirectHomebaseByCoachCoachee";
  static const String createCoachingGuide = "api/CoachingGuide/CreateCoachingGuide";
  static const String getCoachingGuideAll = "api/CoachingGuide/GetCoachingGuideAll";
  static const String getCoachingGuideByCoachingGuideID = "api/CoachingGuide/GetCoachingGuideByCoachingGuideID";
  static const String editCoachingGuideByCoachingGuideID = "api/CoachingGuide/EditCoachingGuideByCoachingGuideID";
  static const String deleteCoachingGuideByCoachingGuideID = "api/CoachingGuide/DeleteCoachingGuideByCoachingGuideID";
  static const String submitCoachingWithAttachments = "api/CoachingActivityDetail/CreateCoachingAttachmentWithEmployeeID";
  static const String getCoachingAttachments = "api/CoachingActivityDetail/GetCoachingAttachmentByHeaderID";
  static const String createDetailAskCoachingWithAttachment = "api/CoachingActivityDetail/CreateDetailAskCoachingWithAttachment";
  static const String createGiveCoachingWithAttachment = "api/CoachingActivityHeader/CreateGiveCoachingWithAttachment";
  static const String downloadAttachment = "api/CoachingAttahment/Download/";
  //static const String deleteFileAttachmentByformattedFileName = "api/CoachingAttahment/DeleteFileAttachmentByformattedFileName";
  static const String getCoachingLinkByHeaderID = "api/CoachingActivityDetail/GetLinkByHeaderID";
  static const String getCoachingAttachmentByHeaderID = "api/CoachingAttahment/GetCoachingAttachmentByHeaderID";
  static const String createCoachingAttachmentWithEmployeeID = "api/CoachingAttahment/CreateCoachingAttachmentWithEmployeeID";

  //pip
  static const String eligiblePIPRequest = "api/PIPRequest/PIP/Eligible";
  static const String validationCoaching = "api/PIPRequest/GetValidasiCoaching";
  static const String getPIPRequest = "api/PIPRequest/PIP/Inquiry";
  static const String createPIPRequest = "api/PIPRequest/PIP";
  static const String deletePipRequest = "api/PIPRequest/PIP";
  static const String autoAddKpi = "api/PIPRequest/KPI-Activity/BatchUpsert";
  static const String getPipKpiActivity = "api/PIPRequest/KPI-Activity/Inquiry";
  static const String getPipDevelopment = "api/Development/Inquiry";
  static const String getDevelopmentGuide = "api/Development/DevelopmentGuide";
  static const String getPipDevelopmentTop = "api/Development/InquiryTop";
  static const String getPipCompetencyCatalog = "api/GeneralMasterCompetencyCatalog/Inquiry";
  static const String submitDataPIP = "api/PIPRequest/submit";
  static const String getPipKpiProject = "api/GeneralMasterKPIProject/Inquiry";
  static const String getKpiCatalog = "api/GeneralMasterKPICatalog/Inquiry";
  static const String pipKpiBatchUpsert = "api/PIPRequest/KPI-Activity/BatchUpsert";
  static const String deleteKPIItem = "api/PIPRequest/DeleteKPIItem";
  static const String updateKPIItem = "api/PIPRequest/UpdateKPIItem";
  static const String deleteKeyActivity = "api/PIPRequest/DeleteKeyActivity";
  static const String updateKeyActivity = "api/PIPRequest/UpdateKeyActivity";
  static const String deleteDevelopment = "api/PIPRequest/DeleteDevelopment";
  static const String pipRequestByPIPNo = "api/PIPRequest/PIPRequestByPIPNo";
  static const String pipRequestByPIPCode = "api/PIPRequest/PIPRequestByPIPCode";
  static const String pipApprove = "api/PIPRequest/Approve";
  static const String pipReject = "api/PIPRequest/Reject";
  static const String pipAskForRevision = "api/PIPRequest/AskForRevision";
  static const String pipBatchUpdate = "api/PIPActivity/BatchUpdate";
  static const String pipUpdateTimeFrame = "api/PIPRequest/PIPUpdateTimeFrame";
  static const String pipUpdatePassRemarks = "api/PIPRequest/PIPUpdatePassRemarks";
  static const String pipbatchUpsertDevelopment = "api/Development/BatchUpsert";
  static const String pipCloseMonitoring = "api/PIPRequest/Monitoring/Closing";
  static const String pipSubmitMonitoring = "api/PIPRequest/NotifPIPIPDSubmitMonitoring";
  static const String batchSubmitAchFinal = "api/PIPActivity/BatchUpdate";
  static const String submitAchFinal = "api/PIPRequest/Final/Submit";
  static const String batchapproveAchFinal = "api/PIPActivity/BatchUpdate";
  static const String approveAchFinal = "api/PIPRequest/Final/Approve";
  static const String getPipActivityDb = "api/PIPRequest/PIPActivityByActivityID";
  static const String getPipActivityByKPIPlanID = "api/PIPRequest/PIPActivityByKPIPlanID";
  static const String getPipRangeRating = "api/GeneralMasterRangeRating/Inquiry";
  static const String getPeriodeCycle = "api/GeneralMasterPeriodCycle/Inquiry";
  static const String pipKpiUpdate = "api/PIPKPI/BatchUpdate";

  //assigment
  static const String getAssignment = "api/IPDRequest/GetAssignmentByID";

  //integration LMS
  static const String getRerunCourse = "api/RerunCourse/GetRerunCourseByCompetencyIDAndName";
  static const String getCompetency = "api/Development/CompetencyByEmployeeID";
  static const String getLearningPlan = "api/LMS/LearningPlan";
  static const String postLearningPlan = "api/LMS/LearningPlan";
  static const String getDevelopmentCompetency = "api/Development/DevelopmentByLearningCatalogAndEmployeeID";
}
