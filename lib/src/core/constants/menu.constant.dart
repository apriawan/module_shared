class MenuConstant {
  static const List<Map<String, dynamic>> listMenu = [
    // {'title': 'Cuti', 'image': IconConstant.icCuti, 'route': Routes.leave, 'isActive': true, 'sequence': 0},
    // {'title': 'Dinas', 'image': IconConstant.icTravel, 'route': '', 'isActive': false, 'sequence': 1},
    // {'title': 'Deklarasi', 'image': IconConstant.icDeclaration, 'route': '', 'isActive': false, 'sequence': 2},
    // {'title': 'Izin', 'image': IconConstant.icIzin, 'route': Routes.permission, 'isActive': true, 'sequence': 3},
    // {'title': 'Lembur', 'image': IconConstant.icLembur, 'route': '', 'isActive': false, 'sequence': 4},
    // {'title': 'Heregistrasi', 'image': IconConstant.icHeregist, 'route': Routes.heregistrasi, 'isActive': false, 'sequence': 5},
    // {'title': 'Service Center', 'image': IconConstant.icBantuan, 'route': Routes.serviceCenter, 'isActive': true, 'sequence': 6},
    // {'title': 'IPD', 'image': IconConstant.icIpd, 'route': '', 'isActive': false, 'sequence': 7},
    // {'title': 'PIP', 'image': IconConstant.icPip, 'route': '', 'isActive': false, 'sequence': 8},
    // {'title': 'Probation', 'image': IconConstant.icProbation, 'route': '', 'isActive': false, 'sequence': 9},
    // {'title': 'Coaching', 'image': IconConstant.icCoaching, 'route': Routes.coaching, 'isActive': false, 'sequence': 10},
    // {'title': 'FeedBack', 'image': IconConstant.icFeedback, 'route': Routes.feedback, 'isActive': false, 'sequence': 11},
    // {'title': 'Nikah', 'image': IconConstant.icMarriage, 'route': Routes.marriage, 'isActive': true, 'sequence': 12},
    // {'title': 'Duka', 'image': IconConstant.icGrief, 'route': Routes.grief, 'isActive': true, 'sequence': 13},
    // {'title': 'Musibah', 'image': IconConstant.icDisaster, 'route': Routes.disaster, 'isActive': true, 'sequence': 14},
    // {'title': 'Kompensasi Cuti Besar', 'image': IconConstant.icLeaveCompensation, 'route': Routes.greateLeave, 'isActive': true, 'sequence': 15},
  ];
  static const String title = 'title';
  static const String image = 'image';
  static const String route = 'route';
  static const String isActive = 'isActive';
  static const String sequence = 'sequence';
}
