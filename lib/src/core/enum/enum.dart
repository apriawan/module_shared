enum ModuleType {
  general,
  notification,
  leaveHO,
  izin,
  workflow,
  benefitMarriage,
  benefitGrief,
  benefitDisaster,
  benefitGreatLeave,
  heregistrasi,
}

enum TypographyType {
  ultraH1,
  largaH2,
  mediumH3,
  smallH4,
  largeH5,
  mediumH6,
  capitalH7,
  smallH8,
  body,
  small,
  bold,
  italics,
  link,
  paragraph,
  paragraphIndent,
  xtraSmall
}

enum FlavorType { dev, uat, prdChucker, prd }
