import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'color.theme.dart';
import 'text.theme.dart';

class ButtonThemeC {
  static ButtonStyle elevatedButtonStyle = ElevatedButton.styleFrom(
    foregroundColor: Colors.white,
    backgroundColor: ColorTheme.primary500,
    shape: RoundedRectangleBorder(
      borderRadius: const BorderRadius.all(Radius.circular(5)).w,
    ),
    textStyle: TextThemeC.poppins.labelLarge,
  );

  static ButtonStyle elevatedButtonStyleDark = ElevatedButton.styleFrom(
      foregroundColor: Colors.white,
      backgroundColor: ColorTheme.maincolorDark,
      shape: RoundedRectangleBorder(
        borderRadius: const BorderRadius.all(Radius.circular(5)).w,
      ),
      textStyle: TextThemeC.poppins.labelLarge);

  static ButtonStyle outlinedButtonStyle = OutlinedButton.styleFrom(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      textStyle: TextThemeC.poppins.labelLarge);

  static ButtonStyle outlinedButtonStyleDark = OutlinedButton.styleFrom(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      textStyle: TextThemeC.poppins.labelLarge);

  static ButtonStyle textButtonStyle = TextButton.styleFrom(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      textStyle: TextThemeC.poppins.labelLarge);

  static ButtonStyle textButtonStyleDark = TextButton.styleFrom(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      textStyle: TextThemeC.poppins.labelLarge);

  static FloatingActionButtonThemeData floatingButtonThemeData = const FloatingActionButtonThemeData().copyWith(
    foregroundColor: Colors.white,
    backgroundColor: ColorTheme.primary500,
  );

  static FloatingActionButtonThemeData floatingButtonThemeDataDark = const FloatingActionButtonThemeData().copyWith(
    foregroundColor: Colors.white,
    backgroundColor: ColorTheme.maincolorDark,
  );
}
