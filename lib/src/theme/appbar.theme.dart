import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'color.theme.dart';

class AppBarThemeC {
  static AppBarTheme appBar = AppBarTheme(
    color: ColorTheme.primary500,
    foregroundColor: Colors.white,
    titleTextStyle: TextStyle(fontSize: 24.sp),
  );
  static AppBarTheme appBarDark = AppBarTheme(
    color: ColorTheme.maincolorDark,
    foregroundColor: Colors.white,
    titleTextStyle: TextStyle(fontSize: 24.sp),
  );
}
