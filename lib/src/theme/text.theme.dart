import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

// link : https://m2.material.io/design/typography/the-type-system.html#type-scale
class TextThemeC {
  static TextTheme poppins = TextTheme(
    displayLarge: GoogleFonts.poppins(fontSize: 98.sp, fontWeight: FontWeight.w300, letterSpacing: -1.5),
    displayMedium: GoogleFonts.poppins(fontSize: 61.sp, fontWeight: FontWeight.w300, letterSpacing: -0.5),
    displaySmall: GoogleFonts.poppins(fontSize: 49.sp, fontWeight: FontWeight.w400),
    headlineMedium: GoogleFonts.poppins(fontSize: 35.sp, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    headlineSmall: GoogleFonts.poppins(fontSize: 24.sp, fontWeight: FontWeight.w400),
    titleLarge: GoogleFonts.poppins(fontSize: 20.sp, fontWeight: FontWeight.w500, letterSpacing: 0.15),
    titleMedium: GoogleFonts.poppins(fontSize: 16.sp, fontWeight: FontWeight.w400, letterSpacing: 0.15),
    titleSmall: GoogleFonts.poppins(fontSize: 14.sp, fontWeight: FontWeight.w500, letterSpacing: 0.1),
    bodyLarge: GoogleFonts.poppins(fontSize: 16.sp, fontWeight: FontWeight.w400, letterSpacing: 0.5),
    bodyMedium: GoogleFonts.poppins(fontSize: 14.sp, fontWeight: FontWeight.w400, letterSpacing: 0.25),
    labelLarge: GoogleFonts.poppins(fontSize: 14.sp, fontWeight: FontWeight.w500, letterSpacing: 1.25),
    bodySmall: GoogleFonts.poppins(fontSize: 12.sp, fontWeight: FontWeight.w400, letterSpacing: 0.4),
    labelSmall: GoogleFonts.poppins(fontSize: 10.sp, fontWeight: FontWeight.w400, letterSpacing: 1.5),
  );
}
