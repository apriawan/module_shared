import 'package:flutter/material.dart';
import 'color.theme.dart';

class InputDecorationThemeC {
  static InputDecorationTheme inputDecorationTheme = const InputDecorationTheme().copyWith(
      enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: ColorTheme.primary500)),
      errorBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.redAccent)),
      disabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
      focusedErrorBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.redAccent)));

  static InputDecorationTheme inputDecorationThemeDark = const InputDecorationTheme().copyWith(
    enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: ColorTheme.maincolorDark)),
    errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.amber.shade800)),
    disabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
    focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.amber.shade800)),
    errorStyle: TextStyle(
      color: Colors.amber.shade800,
    ),
  );
}
