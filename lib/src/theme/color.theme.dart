import 'package:flutter/material.dart';

class ColorTheme {
  static Color primary100 = '#D5FADD'.toColor();
  static Color primary200 = '#9CDDB7'.toColor();
  static Color primary300 = '#6BCB94'.toColor();
  static Color primary400 = '#39BA70'.toColor();
  static Color primary500 = '#08A94C'.toColor();
  static Color primary600 = '#06873D'.toColor();

  static Color textDark = '#333F47'.toColor();
  static Color lightDark = '#7E93A8'.toColor();
  static Color strokeTertiary = '#E8ECF5'.toColor();

  static Color secondary100 = '#FEEAD2'.toColor();
  static Color secondary200 = '#FCD6A5'.toColor();
  static Color secondary300 = '#FBC177'.toColor();
  static Color secondary400 = '#F9AD4A'.toColor();
  static Color secondary500 = '#F8981D'.toColor();
  static Color secondary600 = '#C67A17'.toColor();

  static Color info100 = '#CDE5F8'.toColor();
  static Color info500 = '#047CDD'.toColor();
  static Color info600 = '#0363B1'.toColor();

  static Color danger100 = '#F7D4D4'.toColor();
  static Color danger500 = '#D62926'.toColor();
  static Color danger600 = '#AB211E'.toColor();

  static Color maincolor = '#08a94c'.toColor();
  static Color maincolorDark = '#06873d'.toColor();
}

extension ColorExtension on String {
  toColor() {
    var hexString = this;
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
