import 'dart:async';
import 'package:chucker_flutter/chucker_flutter.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:module_shared/src/core/abstractions/connect.interface.dart';
import 'package:module_shared/src/core/enum/enum.dart';
import 'package:module_shared/src/model/response.model.dart';
import 'package:module_shared/src/core/constants/bspace.module.dart';
import 'package:module_shared/src/core/constants/webservice.constant.dart';

class Connect implements IConnect {
  factory Connect() {
    return _instance;
  }

  Dio dio = Dio();
  static final Connect _instance = Connect._internal();

  Connect._internal() {
    BaseOptions options = BaseOptions(
        baseUrl: WebServiceConstant.rootUrl,
        receiveDataWhenStatusError: true,
        connectTimeout: const Duration(seconds: 60),
        receiveTimeout: const Duration(seconds: 60),
        headers: {
          'Accept': 'application/json',
          'Ocp-Apim-Subscription-Key': dotenv.env['apim.token'],
        });
    dio = Dio(options);
    dio.interceptors.add(ChuckerDioInterceptor());
    dio.interceptors.add(InterceptorsWrapper(
        onError: (e, handler) {
          switch (e.type) {
            case DioExceptionType.badResponse:
              handler.next(e);
            default:
              handler.next(e);
          }
        },
        onResponse: (e, handler) => handler.next(e)));
  }

  // dio.options.baseu
  // IAuthRepository? repoAuth;
  // Connect() {
  //   getHttpClient.baseUrl = WebServiceConstant.rootUrl;
  //   getHttpClient.addRequestModifier<dynamic>((request) {
  //     request.headers['Accept'] = 'application/json';
  //     request.headers['Ocp-Apim-Subscription-Key'] = ConfigEnvironments.getEnvironments()[EnvConstant.apimToken]!;
  //     return request;
  //   });
  //   getHttpClient.addResponseModifier((request, response) async {
  //     if (response.isOk) {
  //       return response;
  //     } else {
  //       if (response.status.isUnauthorized) {
  //         repoAuth = Get.isRegistered() ? Get.find() : Get.put(AuthRepository());
  //         await repoAuth!.logout();
  //         Get.offAllNamed(Routes.auth);
  //         Get.rawSnackbar(message: SharedTranslate.timeout);
  //       }
  //       if (response.status.isServerError) {
  //         throw 'Server Error';
  //       }
  //       if (response.status.isForbidden) {
  //         throw 'Forbidden';
  //       }
  //       if (response.status.isNotFound) {
  //         throw 'Not Found';
  //       }
  //       if (response.status.connectionError) {
  //         throw 'Connection Error';
  //       }
  //       try {
  //         ApiResponse apiResponse = ApiResponse.fromJson(response.body as Map<String, dynamic>);
  //         if (apiResponse.result!.isError == true) {
  //           throw apiResponse.result!.message!;
  //         } else {
  //           throw response.statusText!;
  //         }
  //       } catch (e) {
  //         throw response.statusText!;
  //       }
  //     }
  //   });
  // }

  @override
  Future<ApiResponse> delete({required ModuleType modul, required String path, required id}) async {
    try {
      Response response = await dio.delete('/${BspaceModule.getRootUrl(moduleType: modul)}/$path/$id');
      ApiResponse apiResponse = ApiResponse.fromJson(response.data as Map<String, dynamic>);
      return apiResponse;
    } on DioException catch (e) {
      return exceptionDio(e);
    } catch (e) {
      throw e.toString();
    }
  }

  @override
  Future<ApiResponse> get({required ModuleType modul, required String path, Map<String, dynamic>? query}) async {
    try {
      Response response = await dio.get('/${BspaceModule.getRootUrl(moduleType: modul)}/$path', queryParameters: query);
      ApiResponse apiResponse = ApiResponse.fromJson(response.data as Map<String, dynamic>);
      return apiResponse;
    } on DioException catch (e) {
      return exceptionDio(e);
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<ApiResponse> patch({required ModuleType modul, required String path, required dynamic body}) async {
    try {
      Response response = await dio.patch('/${BspaceModule.getRootUrl(moduleType: modul)}/$path', data: body);
      ApiResponse apiResponse = ApiResponse.fromJson(response.data as Map<String, dynamic>);
      return apiResponse;
    } on DioException catch (e) {
      return exceptionDio(e);
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<ApiResponse> post({required ModuleType modul, required String path, required dynamic body}) async {
    try {
      Response response = await dio.post('/${BspaceModule.getRootUrl(moduleType: modul)}/$path', data: body);
      ApiResponse apiResponse = ApiResponse.fromJson(response.data as Map<String, dynamic>);
      return apiResponse;
    } on DioException catch (e) {
      return exceptionDio(e);
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<ApiResponse> put({required ModuleType modul, required String path, required dynamic body}) async {
    try {
      Response response = await dio.put('/${BspaceModule.getRootUrl(moduleType: modul)}/$path', data: body);
      ApiResponse apiResponse = ApiResponse.fromJson(response.data as Map<String, dynamic>);
      return apiResponse;
    } on DioException catch (e) {
      return exceptionDio(e);
    } catch (e) {
      rethrow;
    }
  }

  exceptionDio(DioException e) {
    return switch (e.type) {
      DioExceptionType.connectionError => throw 'Connection Error',
      DioExceptionType.badCertificate => throw 'Bad Certificate',
      DioExceptionType.badResponse => throw 'Bad Response',
      DioExceptionType.cancel => throw 'Cancel',
      DioExceptionType.connectionTimeout => throw 'Connection Timeout',
      DioExceptionType.receiveTimeout => throw 'Receive Timeout',
      DioExceptionType.sendTimeout => throw 'Send Timeout',
      DioExceptionType.unknown => throw 'Unknown',
    };
  }
}
