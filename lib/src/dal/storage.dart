import 'package:hive/hive.dart';
import 'package:module_shared/src/core/abstractions/storage.interface.dart';

class Storage implements IStorage {
  final Box _storage;
  Storage({required String storage}) : _storage = Hive.box(storage);

  @override
  Future<String?> read(String key) async {
    String? value = await _storage.get(key);
    return value;
  }

  @override
  Future<void> write({required String key, required String? value}) async {
    await _storage.put(key, value);
  }

  @override
  Future<void> remove({required String key}) async {
    await _storage.delete(key);
  }

  @override
  Future<void> erase() async {
    await _storage.clear();
  }

  @override
  Future<Map<dynamic, dynamic>?> getAllData() async {
    return _storage.toMap();
  }
}
